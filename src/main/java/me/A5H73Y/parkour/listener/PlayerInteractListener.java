package me.A5H73Y.parkour.listener;

import me.A5H73Y.parkour.Parkour;
import me.A5H73Y.parkour.course.Checkpoint;
import me.A5H73Y.parkour.course.CheckpointMethods;
import me.A5H73Y.parkour.course.Course;
import me.A5H73Y.parkour.course.CourseMethods;
import me.A5H73Y.parkour.enums.ParkourMode;
import me.A5H73Y.parkour.player.ParkourSession;
import me.A5H73Y.parkour.player.PlayerMethods;
import me.A5H73Y.parkour.utilities.Utils;
import me.A5H73Y.parkour.utilities.XMaterial;
import me.A5H73Y.parkour.Party.Party;
import me.A5H73Y.parkour.Party.PartyManager;
import me.A5H73Y.parkour.course.CourseInfo;

import org.bukkit.ChatColor;

import java.util.ArrayList;

import org.bukkit.Location;
import org.bukkit.block.Block;
import org.bukkit.block.BlockFace;
import org.bukkit.block.Sign;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.Material;

public class PlayerInteractListener implements Listener {
    private static final String prefix = ChatColor.AQUA + "[Party]" + ChatColor.WHITE + " : ";

    @EventHandler
    public void onInventoryInteract(PlayerInteractEvent event) {
        if (!PlayerMethods.isPlaying(event.getPlayer().getName())) {
            return;
        }

        Player player = event.getPlayer();

        if (!event.getAction().equals(Action.RIGHT_CLICK_BLOCK) && !event.getAction().equals(Action.RIGHT_CLICK_AIR)) {
            return;
        }

        if (!player.isSneaking() && Parkour.getPlugin().getConfig().getBoolean("OnCourse.SneakToInteractItems")) {
            return;
        }

        if (PlayerMethods.isPlayerInTestmode(player.getName())) {
            return;
        }

        if (event.getClickedBlock() != null && event.getClickedBlock().getState() instanceof Sign) {
            return;
        }

        if (Utils.getMaterialInPlayersHand(player) == Parkour.getSettings().getLastCheckpointTool()) {
            if (Utils.delayPlayerEvent(player, 1)) {
                event.setCancelled(true);
                PlayerMethods.playerDie(player);
            }

        } else if (Utils.getMaterialInPlayersHand(player) == Parkour.getSettings().getHideallTool()) {
            if (Utils.delayPlayerEvent(player, 1)) {
                event.setCancelled(true);
                Utils.toggleVisibility(player);
            }

        } else if (Utils.getMaterialInPlayersHand(player) == Parkour.getSettings().getLeaveTool()) {
            if (Utils.delayPlayerEvent(player, 1)) {
                event.setCancelled(true);
                Party p = PartyManager.getPartyPlayerIsIn(player);
                if (p != null) {
                    if (p.getLeader().getUniqueId().equals(player.getUniqueId())) {
                        for (Player players : p.getPlayers()) {
                            PlayerMethods.playerLeave(players);
                        }
                    } else {
                        player.sendMessage(prefix + "You are not the party leader!");
                    }
                } else {
                    PlayerMethods.playerLeave(player);
                }
            }

        } else if (Utils.getMaterialInPlayersHand(player) == Parkour.getSettings().getRestartTool()) {
            if (Utils.delayPlayerEvent(player, 1)) {
                event.setCancelled(true);
                PlayerMethods.restartCourse(player);
            }
        } else if (Utils.getMaterialInPlayersHand(player) == Material.STONE_SWORD) {
            if (Utils.delayPlayerEvent(player, 1)) {
                ParkourSession session = PlayerMethods.getParkourSession(event.getPlayer().getName());
                Course course = session.getCourse();
                if (PartyManager.playerInParty(player)) {
                    if (!CourseInfo.isAllowParties(course.getName())) {
                        player.sendMessage("That course does not allow parties!");
                    }
                    Party p = PartyManager.getPartyPlayerIsIn(player);
                    if (p.getLeader().getUniqueId().equals(player.getUniqueId())) {
                        for (Player players : p.getPlayers()) {
                            CourseMethods.joinCourse(players, course.getName());
                        }
                    } else {
                        player.sendMessage("You are not the party leader!");
                    }
                } else {
                    CourseMethods.joinCourse(player, course.getName());
                }
            }
        }

    }

    @EventHandler
    public void onInventoryInteract_ParkourMode(PlayerInteractEvent event) {
        if (!PlayerMethods.isPlaying(event.getPlayer().getName())) {
            return;
        }

        if (!event.getAction().equals(Action.RIGHT_CLICK_BLOCK) && !event.getAction().equals(Action.RIGHT_CLICK_AIR)
                && !event.getAction().equals(Action.LEFT_CLICK_AIR)
                && !event.getAction().equals(Action.LEFT_CLICK_BLOCK)) {
            return;
        }

        ParkourMode mode = PlayerMethods.getParkourSession(event.getPlayer().getName()).getMode();

        if (mode != ParkourMode.FREEDOM && mode != ParkourMode.ROCKETS) {
            return;
        }

        Player player = event.getPlayer();

        if (PlayerMethods.isPlayerInTestmode(player.getName())) {
            return;
        }

        event.setCancelled(true);

        if (mode == ParkourMode.FREEDOM
                && Utils.getMaterialInPlayersHand(player) == XMaterial.REDSTONE_TORCH.parseMaterial()) {
            if (event.getAction().equals(Action.RIGHT_CLICK_BLOCK)
                    || event.getAction().equals(Action.RIGHT_CLICK_AIR)) {
                PlayerMethods.getParkourSession(player.getName()).getCourse()
                        .setCheckpoint(CheckpointMethods.createCheckpointFromPlayerLocation(player));
                player.sendMessage(Utils.getTranslation("Mode.Freedom.Save"));
            } else {
                player.teleport(PlayerMethods.getParkourSession(player.getName()).getCourse().getCurrentCheckpoint()
                        .getLocation());
                player.sendMessage(Utils.getTranslation("Mode.Freedom.Load"));
            }

        } else if (mode == ParkourMode.ROCKETS
                && Utils.getMaterialInPlayersHand(player) == XMaterial.FIREWORK_ROCKET.parseMaterial()) {
            if (event.getAction().equals(Action.RIGHT_CLICK_BLOCK)) {
                if (Utils.delayPlayerEvent(player, 1)) {
                    PlayerMethods.rocketLaunchPlayer(player);
                }
            }
        }
    }

    @EventHandler
    public void onCheckpointEvent(PlayerInteractEvent event) {
        if (event.getAction() != Action.PHYSICAL || !PlayerMethods.isPlaying(event.getPlayer().getName())) {
            return;
        }

        if (event.getClickedBlock().getType() != XMaterial.fromString(Parkour.getSettings().getCheckpointMaterial())
                .parseMaterial()) {
            return;
        }

        if (Parkour.getPlugin().getConfig().getBoolean("OnCourse.PreventPlateStick")) {
            event.setCancelled(true);
        }

        ParkourSession session = PlayerMethods.getParkourSession(event.getPlayer().getName());
        Course course = session.getCourse();

        if (session.getCheckpoint() == course.getCheckpoints()) {
            return;
        }

        Checkpoint check = course.getCurrentCheckpoint();

        if (check == null) {
            return;
        }

        Location below = event.getClickedBlock().getRelative(BlockFace.DOWN).getLocation();

        if (check.getNextCheckpointX() == below.getBlockX() && check.getNextCheckpointY() == below.getBlockY()
                && check.getNextCheckpointZ() == below.getBlockZ()) {
            Party p = PartyManager.getPartyPlayerIsIn(event.getPlayer());
            if (Parkour.getSettings().isFirstCheckAsStart() && session.getCheckpoint() == 0) {
                session.resetTimeStarted();
                Utils.sendActionBar(event.getPlayer(), Utils.getTranslation("Parkour.TimerStarted", false), true);
            }
            if (p == null) {
                PlayerMethods.increaseCheckpoint(session, event.getPlayer());
            } else {
                ArrayList<Player> players = p.getPlayers();
                for (Player player : players) {
                    session = PlayerMethods.getParkourSession(player.getName());
                    PlayerMethods.increaseCheckpoint(session, player);
                }
            }
        }
    }

    @EventHandler
    public void onAutoStartEvent(PlayerInteractEvent event) {
        if (event.getAction() != Action.PHYSICAL) {
            return;
        }

        if (PlayerMethods.isPlaying(event.getPlayer().getName())) {
            return;
        }

        if (!Parkour.getSettings().isAutoStartEnabled()) {
            return;
        }

        Block below = event.getClickedBlock().getRelative(BlockFace.DOWN);

        if (below.getType() != Parkour.getSettings().getAutoStartMaterial()) {
            return;
        }

        // Prevent a user spamming the joins
        if (!Utils.delayPlayer(event.getPlayer(), 3, false)) {
            return;
        }

        String courseName = CourseMethods.getAutoStartCourse(event.getClickedBlock().getLocation());

        if (courseName != null) {
            CourseMethods.joinCourseButDelayed(event.getPlayer(), courseName,
                    Parkour.getSettings().getAutoStartDelay());
        }
    }
}
