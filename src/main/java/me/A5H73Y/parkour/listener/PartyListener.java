package me.A5H73Y.parkour.listener;

import me.A5H73Y.parkour.Party.InviteManager;
import me.A5H73Y.parkour.Party.Party;
import me.A5H73Y.parkour.Party.PartyManager;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;

import org.bukkit.event.player.PlayerQuitEvent;

public class PartyListener implements Listener {

    @EventHandler
    public void onPlayerLeave(PlayerQuitEvent event) {
        Party p = PartyManager.getPartyPlayerIsIn(event.getPlayer());
        if(p != null) {
        	p.remove(event.getPlayer());
        	if(p.getPlayers().size() == 0) {
        		PartyManager.removeParty(p);
        	}
        }
        InviteManager.removeInvite(event.getPlayer());
    }
}