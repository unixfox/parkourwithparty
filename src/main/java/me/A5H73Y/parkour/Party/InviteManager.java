package me.A5H73Y.parkour.Party;

import org.bukkit.entity.Player;

import java.util.ArrayList;
import java.util.List;

public class InviteManager {

    private static final List<Invite> invites = new ArrayList<Invite>();

    public static boolean addInvite(Player player, Party party) {
        if(containsPlayerInvite(player))
            return false;
        invites.add(new Invite(player, party));
        return true;
    }

    public static Invite getInvite(Player player) {
        for(Invite invite : invites) {
            if(invite.getPlayer().getUniqueId().equals(player.getUniqueId())) {
                return invite;
            }
        }
        return null;
    }

    public static boolean removeInvite(Player player) {
        for(int i = 0; i < invites.size(); i++) {
            if(invites.get(i).getPlayer().getUniqueId().equals(player.getUniqueId())) {
                invites.remove(i);
                return true;
            }
        }
        return false;
    }

    public static void updateInvites() {
        for(int i = 0; i < invites.size(); i++) {
            if(System.currentTimeMillis() - invites.get(i).getTime() > 10000) {
                invites.remove(i);
                i--;
            }
        }
    }

    public static boolean containsPlayerInvite(Player player) {
        for(Invite invite : invites) {
            if(invite.getPlayer().getUniqueId().equals(player.getUniqueId())) {
                return true;
            }
        }
        return false;
    }

    public static int numberOfInvites() {
    	return invites.size();
    }
    
    public static void clear() {
        invites.clear();
    }
}
