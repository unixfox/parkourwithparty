package me.A5H73Y.parkour.Party;

import org.bukkit.entity.Player;

public class Invite {

    private Player player;
    private Party party;
    private double time;

    public Invite(Player player, Party party) {
        this.player = player;
        this.party = party;
        time = System.currentTimeMillis();
    }

    public Player getPlayer() {
        return player;
    }

    public Party getParty() {
        return party;
    }

    public double getTime() {
        return time;
    }
}
