package me.A5H73Y.parkour.Party;

import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.entity.Player;

import me.A5H73Y.parkour.course.Course;
import me.A5H73Y.parkour.course.CourseInfo;
import me.A5H73Y.parkour.course.CourseMethods;
import me.A5H73Y.parkour.player.PlayerMethods;
import net.md_5.bungee.api.ChatColor;

public class PartyCommands {

	private static final String prefix = ChatColor.AQUA + "[Party]" + ChatColor.WHITE + " : ";

	public static boolean commands(Player sender, Command command, String label, String[] args) {
		if (args[0].equalsIgnoreCase("createParty")) {
			if (PartyManager.playerInParty(sender)) {
				sender.sendMessage(prefix + "You are already in a party!");
				return true;
			}

			Party party = new Party(sender);
			if (PartyManager.add(party)) {
				sender.sendMessage(prefix + "You have created a party!");
			} else {
				sender.sendMessage(prefix + "You cannot create a party!");
			}
			return true;
		} else if (args[0].equalsIgnoreCase("invitePlayer")) {
			if (args.length != 2)
				sender.sendMessage(prefix + "Please specify the player! Example: /parkour invitePlayer player");
			Player other = Bukkit.getPlayer(args[1]);
			// Sender is not on a course
			if(PlayerMethods.isPlaying(sender.getName())) {
				sender.sendMessage(prefix + "You cannot invite players while on a course");
				return true;
			}
			
			// Player exist
			if (other == null) {
				sender.sendMessage(prefix + "That player does not exist!");
				return true;
			}

			// Other is not sender
			if (other.getUniqueId().equals(sender.getUniqueId())) {
				sender.sendMessage(prefix + "You cannot invite yourself!");
				return true;
			}
			
			// Other is not on a course
			if(PlayerMethods.isPlaying(other.getName())) {
				sender.sendMessage(prefix + "That player is already on a course!");
				return true;
			}

			Party party = PartyManager.getPartyPlayerIsIn(sender);
			// You are in a party
			if (party == null) {
				sender.sendMessage(prefix + "You are not in a party!");
				return true;
			}

			// You are the leader
			if (!party.isLeader(sender)) {
				sender.sendMessage(prefix + "You are not the party leader!");
				return true;
			}

			// The other player is not in party
			if (PartyManager.playerInParty(other)) {
				sender.sendMessage(prefix + "That player is already in a party!");
				return true;
			}

			// Send invite
			InviteManager.addInvite(other, party);
			other.sendMessage(prefix + "You have been invited to a party! Type '/parkour acceptInvite' to join");
			sender.sendMessage(prefix + "You have sent an invite to " + other.getName() + "!");
			return true;
		} else if (args[0].equalsIgnoreCase("acceptInvite")) {
			Invite invite = InviteManager.getInvite(sender);

			// Invite exist
			if (invite == null) {
				sender.sendMessage(prefix + "You do not have a pending invite!");
				return true;
			}

			// Sender is not on a course
			if (PlayerMethods.isPlaying(sender.getName())) {
				sender.sendMessage("You are on a course. Leave the course to accept the invite!");
				return true;
			}
			
			// Dont join parties already on a course
			if (invite.getParty().playersOnCourse()) {
				sender.sendMessage("That party is already on a course. Tell them to leave inorder to join the party!");
				return true;
			}
			
			// Player not already in a party
			if (PartyManager.playerInParty(sender)) {
				sender.sendMessage(prefix + "You are already in a party! Leave your current party to join another!");
				return true;
			}

			// Accept Invite
			invite.getParty().broadcast(prefix + "" + sender.getName() + " has joined your party!");
			sender.sendMessage(prefix + "You have joined the party!");
			invite.getParty().add(sender);
			InviteManager.removeInvite(sender);
			return true;
		} else if (args[0].equalsIgnoreCase("kick")) {
			if (args.length != 2) {
				sender.sendMessage(prefix + "Please specify the player! Example: /parkour kick player");
				return true;
			}
			Player other = Bukkit.getPlayer(args[1]);
			if (other == null) {
				sender.sendMessage(prefix + "Cannot find player " + args[1]);
				return true;
			}

			Party party = PartyManager.getPartyPlayerIsIn(sender);
			if (party == null) {
				sender.sendMessage(prefix + "You are not in a party!");
				return true;
			}

			if (!party.isLeader(sender)) {
				sender.sendMessage(prefix + "You are not the leader!");
				return true;
			}

			if (other.getUniqueId().equals(sender.getUniqueId())) {
				sender.sendMessage(
						prefix + "You cannot kick yourself! Type '/parkour leaveParty' to leave the party!");
				return true;
			}

			if (!party.contains(other)) {
				sender.sendMessage(prefix + "" + other.getName() + " is not in your party!");
				return true;
			}

			party.remove(other);
			other.sendMessage(prefix + "You have been kicked from the group!");
			party.broadcast(prefix + "Player " + other.getName() + " has been kicked!");
			return true;
		} else if (args[0].equalsIgnoreCase("leaveParty")) {
			Party p = PartyManager.getPartyPlayerIsIn(sender);
			if (p == null) {
				sender.sendMessage(prefix + "You are not in a party!");
				return true;
			}
			Player lastLeader = p.getLeader();
			p.remove(sender);
			if (p.isEmpty()) {
				PartyManager.removeParty(p);
				sender.sendMessage(prefix + "You have left the party!");
				return true;
			} else {
				p.broadcast(prefix + "" + sender.getName() + " has left the party!");
				if(lastLeader.getName().equals(p.getLeader().getName())) {p.getLeader().sendMessage(prefix + "You are now the party leader!");}
				sender.sendMessage(prefix + "You have left the party!");
				return true;
			}
		} else if (args[0].equalsIgnoreCase("allowParties")) {
			if(!sender.hasPermission("Parkour.Admin")) {
				sender.sendMessage(prefix + "You do not have access to that command!");
				return true;
			}
			
			if(args.length != 3) {
				sender.sendMessage(prefix + "Invalid number of arguments!");
				return true;
			}
			
			if(!CourseMethods.exist(args[1])) {
				sender.sendMessage(prefix + args[1] + " is not a valid course!");
				return true;
			}
			boolean value = false;
			try {
				value = Boolean.parseBoolean(args[2]);
			} catch (Exception e) {
				sender.sendMessage(prefix + args[1] + " is not 'true' or 'false'");
				return true;
			}
			CourseInfo.setAllowParties(args[1], value);
			sender.sendMessage(prefix + args[1] + " is now set to " + value);
			return true;

		}
		return false;
	}
}
