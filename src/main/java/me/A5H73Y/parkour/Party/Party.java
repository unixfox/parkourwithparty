package me.A5H73Y.parkour.Party;

import org.bukkit.entity.Player;

import me.A5H73Y.parkour.player.PlayerMethods;

import java.util.ArrayList;

public class Party {

	private int checkpoint = 0;
	
    public ArrayList<Player> players;

    public Party(Player player) {
        players = new ArrayList<Player>();
        players.add(player);
    }

    public boolean add(Player player) {
        if(contains(player))
            return false;
        players.add(player);
        return true;
    }

    public boolean remove(Player player) {
        for(int i = 0; i < players.size(); i++) {
            if(players.get(i).getUniqueId().equals(player.getUniqueId())) {
                players.remove(i);
                return true;
            }
        }
        return false;
    }

    public boolean isLeader(Player player) {
        if(players.get(0).getUniqueId().equals(player.getUniqueId()))
            return true;
        return false;
    }

    public boolean contains(Player player) {
       for(Player p : players) {
           if(p.getUniqueId().equals(player.getUniqueId())) {
               return true;
           }
       }
       return false;
    }

    public void broadcast(String message) {
        for(Player p : players) {
            p.sendMessage(message);
        }
    }

    public boolean isEmpty() {
        return players.size() == 0;
    }

    public Player getLeader() {
        return players.get(0);
    }

    public boolean playersOnCourse() {
    	for (Player p : players) {
    		if(PlayerMethods.isPlaying(p.getName())) {
    			return true;
    		}
    	}
    	return false;
    }
    
    @Override
    public boolean equals(Object o) {
        if(!(o instanceof Party))
            return false;
        Party p = (Party)o;
        if(p.toString().equals(this.toString()))
            return true;
        else
            return false;
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        for(Player player : players) {
            builder.append(player.getName()).append("\n");
        }
        return builder.toString();
    }

	public int getCheckpoint() {
		return checkpoint;
	}

	public void setCheckpoint(int checkpoint) {
		this.checkpoint = checkpoint;
	}

	public ArrayList<Player> getPlayers() {
		return players;
	}
}
