package me.A5H73Y.parkour.Party;

import org.bukkit.entity.Player;

import me.A5H73Y.parkour.player.PlayerMethods;

import java.util.ArrayList;
import java.util.List;

public class PartyManager {

	private static final List<Party> parties = new ArrayList<Party>();

	// Adds a party to the list
	public static boolean add(Party party) {
		if (contains(party))
			return false;
		parties.add(party);
		return true;
	}

	// Find if a player is in a party
	public static boolean playerInParty(Player player) {
		for (Party p : parties) {
			if (p.contains(player)) {
				return true;
			}
		}
		return false;
	}

	public static Party getPartyPlayerIsIn(Player player) {
		for (Party p : parties) {
			if (p.contains(player)) {
				return p;
			}
		}
		return null;
	}

	public static void removeParty(Party party) {
		for (int i = 0; i < parties.size(); i++) {
			if (parties.get(i).equals(party)) {
				parties.remove(i);
				return;
			}
		}
	}

	public static int numberOfParties() {
		return parties.size();
	}

	// Find if a party already exist
	public static boolean contains(Party party) {
		return parties.contains(party);
	}
}
